package com.company;

import java.io.*;

public class Main {

    public static void main(String[] args) {

        //You should paste path to .csv file with prepared data first name_of_file , second name_of_disc separated by comma
        File fileToMap = new File (args[0]);
        try (BufferedReader br = new BufferedReader(new FileReader(fileToMap))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineSplited = line.split(",");
                if(lineSplited.length>=2){
                String disc = lineSplited[1];
                System.out.println(disc);
                String file = lineSplited[0];
                System.out.println(file);
                File f = new File(fileToMap.getAbsolutePath().replaceAll(".csv","")+"\\"+disc+"\\"+file);
                f.getParentFile().mkdirs();
                f.createNewFile();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}

